import RectangleRender from "./RectangleRender";
import Board from "../Board";

export default class GroundBlockRender extends RectangleRender{
    render(board: Board): void {
        board.context.fillStyle = '#2f6335';
        super.render(board);
    }

}