import Renderable from "../Renderable";

export default abstract class BoardObject {
    public abstract getRenderable(): Renderable;
    public abstract execute(): void;

}