import GameObject from "./GameObject";
import Renderable from "../Renderable";
import PlayerRender from "../Renders/PlayerRender";
import Rectangle from "../Primitives/Rectangle";

export default class Player extends GameObject {
    public speed = 10;
    protected jumping: boolean = false;
    protected currentHeight: number = 0;
    protected movingRight: boolean = false;
    protected movingLeft: boolean = false;
    readonly jumpHeight: number = 200;

    protected _primitive = new Rectangle(this.point, 40, 40);

    getRenderable(): Renderable {
        return new PlayerRender(this._primitive);
    }

    public moveRight() {
        if (!this.movingLeft) {
            this.movingRight = true;
        }
    }

    public moveLeft() {
        if (!this.movingRight) {
            this.movingLeft = true;
        }
    }

    public jump() {
        if (this.onGround()) {
            this.jumping = true;
        }
    }

    public stopMoveRight() {
        this.movingRight = false;
    }

    public stopMoveLeft() {
        this.movingLeft = false;
    }

    public stopJump() {
        this.jumping = false;
    }

    private moveRightLogic() {
        if (this.movingRight) {
            this.point.x += this.speed;
        }
    }

    private moveLeftLogic() {
        if (this.movingLeft) {
            this.point.x -= this.speed;
        }
    }

    private jumpLogic() {
        if (this.jumping && this.currentHeight < this.jumpHeight) {
            this.point.y -= 10;
            this.currentHeight += 10;
        } else {
            this.jumping = false;
            this.currentHeight = 0;
        }
    }

    execute(): void {
        this.jumpLogic();
        this.moveRightLogic();
        this.moveLeftLogic();
    }
}