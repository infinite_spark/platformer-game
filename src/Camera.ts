import GameObject from "./BoardObjects/GameObject";
import Board from "./Board";
import Renderable from "./Renderable";
import Point from "./Primitives/Point";
import Gravity from "./Gravity";

declare var board: Board;
declare var gravity: Gravity;

export default class Camera implements Renderable{
    protected watchOn: GameObject;

    constructor(watchOn: GameObject) {
        this.watchOn = watchOn;
    }

    public work() {
        if ((this.point.x + this.width) >= board.virtualWidth) {
            board.offset.x += this.watchOn.speed;
        }
        if (this.point.x <= board.virtualWidth - board.width) {
            board.offset.x -= this.watchOn.speed;
        }

        if ((this.point.y) < board.virtualHeight - board.height) {
            board.offset.y += gravity.forceValue;
        }
        if (this.point.y + this.height >= board.virtualHeight) {
            board.offset.y -= gravity.forceValue;
        }
    }

    get width() {
        return board.width;
    }

    get height() {
        return board.height;
    }

    get point(): Point {
        let x = this.watchOn.getPrimitive().center().x - this.width / 2;
        let y = this.watchOn.getPrimitive().center().y - this.height / 2;

        return new Point(x, y);
    }

    render(board: Board): void {
        board.context.beginPath();
        board.context.lineWidth = 3;
        board.context.strokeStyle = "red";
        board.context.rect(this.point.x - board.offset.x, this.point.y + board.offset.y, this.width, this.height);
        board.context.stroke();
        board.context.fillStyle = 'red';
    }
}