import Map from "./Map";
import GroundBlock from "../BoardObjects/GroundBlock";
import Point from "../Primitives/Point";
import GameObject from "../BoardObjects/GameObject";
import DecorationObject from "../BoardObjects/Decorations/DecorationObject";
import GroundDecoration from "../BoardObjects/Decorations/GroundDecoration";
import BackgroundDecoration from "../BoardObjects/Decorations/BackgroundDecoration";

export default class Level1 implements Map{
    getObjects(): GameObject[] {
        return [
            new GroundBlock(new Point(0, 500), 500),
            new GroundBlock(new Point(550, 500), 200),
            new GroundBlock(new Point(800, 500), 400),
            new GroundBlock(new Point(1200, 500), 300),
            new GroundBlock(new Point(1500, 400), 600),
            new GroundBlock(new Point(1700, 300), 600),
            new GroundBlock(new Point(1850, 200), 600),
            new GroundBlock(new Point(2000, 100), 600),
            new GroundBlock(new Point(3000, 500), 300),
        ];
    }

    getDecorations(): DecorationObject[] {
        return [
            new BackgroundDecoration(),
            new GroundDecoration()
        ];
    }
}