import GameObject from "../BoardObjects/GameObject";
import DecorationObject from "../BoardObjects/Decorations/DecorationObject";

export default interface Map {
    getDecorations(): DecorationObject[];
    getObjects(): GameObject[];
}